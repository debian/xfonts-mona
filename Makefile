#  Makefile for Mona font
#  by 1@2ch, 2002, public domain
#  $Id: Makefile,v 1.10 2003/09/10 02:54:10 s42335 Exp $

VERSION = 2.90

## Machine dependent

SED_CMD		= sed
PERL_CMD	= perl

# for XFree86
GZIP_CMD	= gzip
GZIP_SUFFIX	= gz

# for traditional Xserver (SunOS, HP-UX, Tru64 etc.)
#GZIP_CMD	= compress
#GZIP_SUFFIX	= Z

DESTDIR		=
X11BINDIR	= /usr/X11R6/bin
X11FONTDIR	= $(DESTDIR)/usr/X11R6/lib/X11/fonts/local
BDFTOPCF	= $(X11BINDIR)/bdftopcf
MKDIRHIER	= $(X11BINDIR)/mkdirhier	# 'mkdir -p' may also work.
MKFONTDIR	= $(X11BINDIR)/mkfontdir

MY_RPM_ROOT	= /tmp/rpm-root

## Other macros

BIT2BDF	= $(PERL_CMD) tools/bit2bdf
BDFMERGE= $(PERL_CMD) tools/bdfmerge
MKBOLD	= $(PERL_CMD) tools/mkbold -V
MKITALIC= $(PERL_CMD) tools/mkitalic -V
ADJUST	= $(PERL_CMD) tools/adjust
JIS2UNICODE= $(PERL_CMD) ttftools/jis2unicode -b
MKISO8859= $(SED_CMD) 's/@@REGISTRY@@/ISO8859/g;s/@@ENCODING@@/1/g;'
MKJISX0201= $(SED_CMD) 's/@@REGISTRY@@/JISX0201.1976/g;s/@@ENCODING@@/0/g;'
MKJISX0208= $(SED_CMD) 's/@@REGISTRY@@/JISX0208.1990/g;s/@@ENCODING@@/0/g;'
MKISO10646= $(SED_CMD) 's/@@REGISTRY@@/ISO10646/g;s/@@ENCODING@@/1/g;'

BDF =	mona6x12a mona6x12aB mona6x12aI mona6x12aBI \
	mona6x12r mona6x12rB mona6x12rI mona6x12rBI \
	mona7x14a mona7x14aB mona7x14aI mona7x14aBI \
	mona7x14r mona7x14rB mona7x14rI mona7x14rBI \
	mona8x16a mona8x16aB mona8x16aI mona8x16aBI \
	mona8x16r mona8x16rB mona8x16rI mona8x16rBI \
	monak12 monak12B monak12I monak12BI \
	monau12 monau12B monau12I monau12BI \
	monak14 monak14B monak14I monak14BI \
	monau14 monau14B monau14I monau14BI \
	monak16 monak16B monak16I monak16BI \
	monau16 monau16B monau16I monau16BI

## Dependencies

bdf:	bdf-normal bdf-bold bdf-italic bdf-bold-italic

bdf-normal:	dist/mona6x12a.bdf dist/mona6x12r.bdf \
		dist/mona7x14a.bdf dist/mona7x14r.bdf \
		dist/mona8x16a.bdf dist/mona8x16r.bdf \
		dist/monak12.bdf \
		dist/monau12.bdf \
		dist/monak14.bdf \
		dist/monau14.bdf \
		dist/monak16.bdf \
		dist/monau16.bdf
bdf-bold:	dist/mona6x12aB.bdf dist/mona6x12rB.bdf \
		dist/mona7x14aB.bdf dist/mona7x14rB.bdf \
		dist/mona8x16aB.bdf dist/mona8x16rB.bdf \
		dist/monak12B.bdf \
		dist/monau12B.bdf \
		dist/monak14B.bdf \
		dist/monau14B.bdf \
		dist/monak16B.bdf \
		dist/monau16B.bdf
bdf-italic:	dist/mona6x12aI.bdf dist/mona6x12rI.bdf \
		dist/mona7x14aI.bdf dist/mona7x14rI.bdf \
		dist/mona8x16aI.bdf dist/mona8x16rI.bdf \
		dist/monak12I.bdf \
		dist/monau12I.bdf \
		dist/monak14I.bdf \
		dist/monau14I.bdf \
		dist/monak16I.bdf \
		dist/monau16I.bdf
bdf-bold-italic:dist/mona6x12aBI.bdf dist/mona6x12rBI.bdf \
		dist/mona7x14aBI.bdf dist/mona7x14rBI.bdf \
		dist/mona8x16aBI.bdf dist/mona8x16rBI.bdf \
		dist/monak12BI.bdf \
		dist/monau12BI.bdf \
		dist/monak14BI.bdf \
		dist/monau14BI.bdf \
		dist/monak16BI.bdf \
		dist/monau16BI.bdf

# primary bitmaps

# ASCII (iso8859)
dist/mona6x12a.bdf: src/mona6x12.bit0 src/mona6x12a.bit1
	$(BDFMERGE) src/mona6x12.bit0 src/mona6x12a.bit1 | \
		$(MKISO8859) | $(BIT2BDF) > dist/mona6x12a.bdf
dist/mona7x14a.bdf: src/mona7x14.bit0 src/mona7x14a.bit1
	$(BDFMERGE) src/mona7x14.bit0 src/mona7x14a.bit1 | \
		$(MKISO8859) | $(BIT2BDF) > dist/mona7x14a.bdf
dist/mona8x16a.bdf: src/mona8x16.bit0 src/mona8x16a.bit1
	$(BDFMERGE) src/mona8x16.bit0 src/mona8x16a.bit1 | \
		$(MKISO8859) | $(BIT2BDF) > dist/mona8x16a.bdf

# HANKAKU-KANA (jisx0201.1976)
dist/mona6x12r.bdf: src/mona6x12.bit0 src/mona6x12r.bit1
	$(BDFMERGE) src/mona6x12.bit0 src/mona6x12r.bit1 | \
		$(MKJISX0201) | $(BIT2BDF) > dist/mona6x12r.bdf
dist/mona7x14r.bdf: src/mona7x14.bit0 src/mona7x14r.bit1
	$(BDFMERGE) src/mona7x14.bit0 src/mona7x14r.bit1 | \
		$(MKJISX0201) | $(BIT2BDF) > dist/mona7x14r.bdf
dist/mona8x16r.bdf: src/mona8x16.bit0 src/mona8x16r.bit1
	$(BDFMERGE) src/mona8x16.bit0 src/mona8x16r.bit1 | \
		$(MKJISX0201) | $(BIT2BDF) > dist/mona8x16r.bdf

# KANJI (jisx0208.1990)
dist/monak12.bdf: src/monak12-base.bit src/monak12-ext.bit
	$(BDFMERGE) src/monak12-base.bit src/monak12-ext.bit | \
		$(MKJISX0208) | $(BIT2BDF) > dist/monak12.bdf
dist/monak14.bdf: src/monak14-base.bit src/monak14-ext.bit
	$(BDFMERGE) src/monak14-base.bit src/monak14-ext.bit | \
		$(MKJISX0208) | $(BIT2BDF) > dist/monak14.bdf
dist/monak16.bdf: src/monak16-base.bit src/monak16-ext.bit
	$(BDFMERGE) src/monak16-base.bit src/monak16-ext.bit | \
		$(MKJISX0208) | $(BIT2BDF) > dist/monak16.bdf

# UNICODE (iso10646)
dist/monau12.bdf: src/mona6x12.bit0 src/mona6x12a.bit1 src/mona6x12r.bit1 \
		   src/monak12-base.bit src/monak12-ext.bit src/monak12-uext.bit
	$(JIS2UNICODE) src/monak12-base.bit src/monak12-ext.bit src/mona6x12r.bit1 | \
		$(MKISO10646) | \
		$(BDFMERGE) - src/monak12-uext.bit src/mona6x12.bit0 src/mona6x12a.bit1 |\
		$(BIT2BDF) > dist/monau12.bdf
dist/monau14.bdf: src/mona7x14.bit0 src/mona7x14a.bit1 src/mona7x14r.bit1 \
		   src/monak14-base.bit src/monak14-ext.bit src/monak14-uext.bit
	$(JIS2UNICODE) src/monak14-base.bit src/monak14-ext.bit src/mona7x14r.bit1 | \
		$(MKISO10646) | \
		$(BDFMERGE) - src/monak14-uext.bit src/mona7x14.bit0 src/mona7x14a.bit1 |\
		$(BIT2BDF) > dist/monau14.bdf
dist/monau16.bdf: src/mona8x16.bit0 src/mona8x16a.bit1 src/mona8x16r.bit1 \
		   src/monak16-base.bit src/monak16-ext.bit src/monak16-uext.bit
	$(JIS2UNICODE) src/monak16-base.bit src/monak16-ext.bit src/mona8x16r.bit1 | \
		$(MKISO10646) | \
		$(BDFMERGE) - src/monak16-uext.bit src/mona8x16.bit0 src/mona8x16a.bit1 |\
		$(BIT2BDF) > dist/monau16.bdf


# modified bitmaps

dist/mona6x12aB.bdf: dist/mona6x12a.bdf
	$(MKBOLD) dist/mona6x12a.bdf > dist/mona6x12aB.bdf
dist/mona6x12rB.bdf: dist/mona6x12r.bdf
	$(MKBOLD) dist/mona6x12r.bdf > dist/mona6x12rB.bdf
dist/mona6x12aI.bdf: dist/mona6x12a.bdf
	$(MKITALIC) dist/mona6x12a.bdf > dist/mona6x12aI.bdf
dist/mona6x12rI.bdf: dist/mona6x12r.bdf
	$(MKITALIC) dist/mona6x12r.bdf > dist/mona6x12rI.bdf
dist/mona6x12aBI.bdf: dist/mona6x12aI.bdf
	$(MKBOLD) dist/mona6x12aI.bdf > dist/mona6x12aBI.bdf
dist/mona6x12rBI.bdf: dist/mona6x12rI.bdf
	$(MKBOLD) dist/mona6x12rI.bdf > dist/mona6x12rBI.bdf

dist/mona7x14aB.bdf: dist/mona7x14a.bdf
	$(MKBOLD) dist/mona7x14a.bdf > dist/mona7x14aB.bdf
dist/mona7x14rB.bdf: dist/mona7x14r.bdf
	$(MKBOLD) dist/mona7x14r.bdf > dist/mona7x14rB.bdf
dist/mona7x14aI.bdf: dist/mona7x14a.bdf
	$(MKITALIC) dist/mona7x14a.bdf > dist/mona7x14aI.bdf
dist/mona7x14rI.bdf: dist/mona7x14r.bdf
	$(MKITALIC) dist/mona7x14r.bdf > dist/mona7x14rI.bdf
dist/mona7x14aBI.bdf: dist/mona7x14aI.bdf
	$(MKBOLD) dist/mona7x14aI.bdf > dist/mona7x14aBI.bdf
dist/mona7x14rBI.bdf: dist/mona7x14rI.bdf
	$(MKBOLD) dist/mona7x14rI.bdf > dist/mona7x14rBI.bdf

dist/mona8x16aB.bdf: dist/mona8x16a.bdf
	$(MKBOLD) dist/mona8x16a.bdf > dist/mona8x16aB.bdf
dist/mona8x16rB.bdf: dist/mona8x16r.bdf
	$(MKBOLD) dist/mona8x16r.bdf > dist/mona8x16rB.bdf
dist/mona8x16aI.bdf: dist/mona8x16a.bdf
	$(MKITALIC) dist/mona8x16a.bdf > dist/mona8x16aI.bdf
dist/mona8x16rI.bdf: dist/mona8x16r.bdf
	$(MKITALIC) dist/mona8x16r.bdf > dist/mona8x16rI.bdf
dist/mona8x16aBI.bdf: dist/mona8x16aI.bdf
	$(MKBOLD) dist/mona8x16aI.bdf > dist/mona8x16aBI.bdf
dist/mona8x16rBI.bdf: dist/mona8x16rI.bdf
	$(MKBOLD) dist/mona8x16rI.bdf > dist/mona8x16rBI.bdf

dist/monak12B.bdf: dist/monak12.bdf
	$(MKBOLD) dist/monak12.bdf > dist/monak12B.bdf
dist/monak12I.bdf: dist/monak12.bdf
	$(MKITALIC) dist/monak12.bdf > dist/monak12I.bdf
dist/monak12BI.bdf: dist/monak12I.bdf
	$(MKBOLD) dist/monak12I.bdf > dist/monak12BI.bdf
dist/monau12B.bdf: dist/monau12.bdf
	$(MKBOLD) dist/monau12.bdf > dist/monau12B.bdf
dist/monau12I.bdf: dist/monau12.bdf
	$(MKITALIC) dist/monau12.bdf > dist/monau12I.bdf
dist/monau12BI.bdf: dist/monau12I.bdf
	$(MKBOLD) dist/monau12I.bdf > dist/monau12BI.bdf

dist/monak14B.bdf: dist/monak14.bdf
	$(MKBOLD) dist/monak14.bdf > dist/monak14B.bdf
dist/monak14I.bdf: dist/monak14.bdf
	$(MKITALIC) dist/monak14.bdf > dist/monak14I.bdf
dist/monak14BI.bdf: dist/monak14I.bdf
	$(MKBOLD) dist/monak14I.bdf > dist/monak14BI.bdf
dist/monau14B.bdf: dist/monau14.bdf
	$(MKBOLD) dist/monau14.bdf > dist/monau14B.bdf
dist/monau14I.bdf: dist/monau14.bdf
	$(MKITALIC) dist/monau14.bdf > dist/monau14I.bdf
dist/monau14BI.bdf: dist/monau14I.bdf
	$(MKBOLD) dist/monau14I.bdf > dist/monau14BI.bdf

dist/monak16B.bdf: dist/monak16.bdf
	$(MKBOLD) dist/monak16.bdf > dist/monak16B.bdf
dist/monak16I.bdf: dist/monak16.bdf
	$(MKITALIC) dist/monak16.bdf > dist/monak16I.bdf
dist/monak16BI.bdf: dist/monak16I.bdf
	$(MKBOLD) dist/monak16I.bdf > dist/monak16BI.bdf
dist/monau16B.bdf: dist/monau16.bdf
	$(MKBOLD) dist/monau16.bdf > dist/monau16B.bdf
dist/monau16I.bdf: dist/monau16.bdf
	$(MKITALIC) dist/monau16.bdf > dist/monau16I.bdf
dist/monau16BI.bdf: dist/monau16I.bdf
	$(MKBOLD) dist/monau16I.bdf > dist/monau16BI.bdf


##  Utilities

# clean
clean:
	-rm dist/*.bdf dist/fonts.dir *~ .*~
ttfclean:
	cd ttfsrc; make clean

# packing
pack: clean ttfclean
	cd ..; tar c --numeric-owner --exclude CVS --bzip2 --dereference \
		 -f monafont-$(VERSION).tar.bz2 monafont-$(VERSION)
	cd ..; tar c --numeric-owner --exclude CVS --bzip2 --dereference \
		 -f ttftinker-$(VERSION).tar.bz2 ttftinker-$(VERSION)
rpm: clean ttfclean
	cd ..; tar c --numeric-owner --exclude CVS --bzip2 --dereference \
		 -f $(MY_RPM_ROOT)/SOURCES/monafont-$(VERSION).tar.bz2 monafont-$(VERSION)
	rpm -ba monafont.spec

# install
install: bdf
	if [ ! -d $(X11FONTDIR) ]; then \
		$(MKDIRHIER) $(X11FONTDIR); \
	fi
	for i in $(BDF); do \
		$(BDFTOPCF) dist/$$i.bdf | \
		$(GZIP_CMD) -c > $(X11FONTDIR)/$$i.pcf.$(GZIP_SUFFIX); \
	done
	$(MKFONTDIR) $(X11FONTDIR)

# append fonts.alias
install-alias:
	cat fonts.alias.mona >> $(X11FONTDIR)/fonts.alias
