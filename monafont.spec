# $Id: monafont.spec,v 1.6 2003/09/10 02:54:10 s42335 Exp $
Summary: Mona Font
Name: monafont
Version: 2.99
Release: 1
Group: X11/Fonts
Copyright: Public Domain
URL: http://monafont.sourceforge.net/
Source: http://prdownloads.sourceforge.net/monafont/monafont-2.99.tar.bz2
BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Prereq: /usr/X11R6/bin/mkfontdir
Packager: s42335@users.sourceforge.net
Obsoletes: mona-font
Obsoletes: monafonts

%description
Mona Font is a Japanese font suitable for text arts posted in the Ni-channeru BBS.

%description -l ja
モナーフォントはアスキーアートを見るためのフォントです。

%prep
rm -rf ${RPM_BUILD_ROOT}

%setup -q

%build
make

%install
rm -rf ${RPM_BUILD_ROOT}
make install DESTDIR="${RPM_BUILD_ROOT}"
cp fonts.alias.mona ${RPM_BUILD_ROOT}/usr/X11R6/lib/X11/fonts/local

%clean
rm -rf ${RPM_BUILD_ROOT}

%post
cd /usr/X11R6/lib/X11/fonts/local
/usr/X11R6/bin/mkfontdir
cat fonts.alias.mona >> fonts.alias

%postun
cd /usr/X11R6/lib/X11/fonts/local
/usr/X11R6/bin/mkfontdir
mv fonts.alias fonts.alias.rpmsave
sed '/^!! MONAFONT_ALIAS_BEGIN/,/^!! MONAFONT_ALIAS_END/d' fonts.alias.rpmsave > fonts.alias

%files
%defattr(-, root, root)
/usr/X11R6/lib/X11/fonts/local/fonts.alias.mona
/usr/X11R6/lib/X11/fonts/local/mona*.pcf.gz
%doc README.euc
%doc README.ascii

%changelog
* Tue Sep 7 2003 by 1 <s42335@excite.co.jp>
- Update to 2.99.

* Tue Aug 5 2003 by 1 <s42335@excite.co.jp>
- Update to 2.30pre2.

* Thu Dec 22 2002 by 1 <s42335@users.sourceforge.net>
- Modified %post, %postun.

* Thu Dec 20 2002 by 1 <s42335@users.sourceforge.net>
- Updated to 2.22 and small fixes.

* Thu Jun 22 2002 by 1 <s42335@excite.co.jp>
- Update to 2.21.

* Thu May 14 2002 by 1 <s42335@excite.co.jp>
- Update to 2.2.

* Thu May 2 2002 by 1 <s42335@excite.co.jp>
- Update to 2.12.

* Fri Apr 5 2002 nanasisan@onakaippai <>
- Update to 2.11.

* Sun Jul 22 2001 Yukihiro Nakai <nakai@gnome.gr.jp>
- Update to 2.03.

* Sat May 18 2001 Yukihiro Nakai <nakai@gnome.gr.jp>
- Initial RPM release.
